import { Component } from '@angular/core';
import { FilterService } from 'primeng/api';
import { CountryService } from './countryservice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {}
