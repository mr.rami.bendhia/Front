import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';

import { InputComponent } from './components/input/input.component';
import { RoutingComponent } from './components/routing/routing.component';
import { HomeComponent } from './components/home/home.component';
import {AccountComponent} from './components/account/account.component';
import {OperationComponent } from './components/account/operation/operation.component';

const routes: Routes = [
  { path: 'input', component: InputComponent },
  { path: 'routing', component: RoutingComponent },
  { path: 'home', component: HomeComponent },
  { path: 'account', component: AccountComponent },
  { path: 'operation', component: OperationComponent },

  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
