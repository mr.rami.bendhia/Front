import {Component, OnInit} from '@angular/core';
import {DialogService} from 'primeng/dynamicdialog';
import {OperationComponent} from './operation/operation.component';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.css'],
    providers: [DialogService],

})
export class AccountComponent implements OnInit {

    constructor(public dialogService: DialogService,
                // public primengConfig: PrimeNGConfig,
                // private  customerService: CustomerService

    ) {
    }

    ngOnInit(): void {

    }


    show() {

        const ref = this.dialogService.open(OperationComponent, {

            header: 'Add Account',


            width: '70%',
            // height: '100%',

        });


    }
}
