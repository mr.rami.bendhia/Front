import {Component, OnInit} from '@angular/core';
import {MessageService, PrimeNGConfig} from 'primeng/api';

interface City {
    name: string;
    code: string;
}

@Component({
    selector: 'app-operation',
    templateUrl: './operation.component.html',
    styleUrls: ['./operation.component.css'],
    providers: [MessageService]

})


export class OperationComponent implements OnInit {
    cities: City[];

    constructor(
        private messageService: MessageService,
        private primengConfig: PrimeNGConfig,
    ) {
        this.cities = [
            {name: 'New York', code: 'NY'},
            {name: 'Rome', code: 'RM'},
            {name: 'London', code: 'LDN'},
            {name: 'Istanbul', code: 'IST'},
            {name: 'Paris', code: 'PRS'}
        ];
    }

    ngOnInit(): void {
        this.primengConfig.ripple = true;

    }


    showSuccess() {
        this.messageService.add({severity: 'success', summary: 'Success', detail: 'Ajoute complet'});
    }
  showWarn() {
    this.messageService.add({severity:'warn', summary: 'Warn', detail: 'Modification complet'});
  }

  showError() {
    this.messageService.add({severity:'error', summary: 'Error', detail: 'Suppression complet'});
  }
}
