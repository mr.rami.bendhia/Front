// import { Component, OnInit } from '@angular/core';
import {DialogService} from 'primeng/dynamicdialog';
import {InputComponent} from '../input/input.component';

import {Component, OnInit, ViewChild} from '@angular/core';
import {Customer, Representative} from './customer';
import {CustomerService} from './customerservice';
import {Table} from 'primeng/table';
import {PrimeNGConfig} from 'primeng/api';


@Component({
    selector: 'app-routing',
    templateUrl: './routing.component.html',
    styleUrls: ['./routing.component.css'],
    providers: [DialogService],
})
export class RoutingComponent implements OnInit {
    customers: Customer[];

    selectedCustomers: Customer[];

    representatives: Representative[];

    statuses: any[];

    loading: boolean;

    @ViewChild('dt') table: Table;

    constructor(public dialogService: DialogService,
                // public primengConfig: PrimeNGConfig,
 // private  customerService: CustomerService

    ) {
    }

    ngOnInit(): void {

    }


    show() {

        const ref = this.dialogService.open(InputComponent, {

            header: 'Add Company',


            width: '40%'

        });

    }

}
