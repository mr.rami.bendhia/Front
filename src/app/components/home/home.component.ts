import {Component, OnInit} from '@angular/core';

import {DialogService} from 'primeng/dynamicdialog';
import {InputComponent} from '../input/input.component';
import {MenuItem, PrimeNGConfig} from 'primeng/api';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers: [DialogService],

})
export class HomeComponent implements OnInit {
    visibleSidebar1;

    visibleSidebar2;

    constructor(
        public dialogService: DialogService,
        private primengConfig: PrimeNGConfig,
    ) {
    }

    items: MenuItem[];

    ngOnInit() {
        this.primengConfig.ripple = true;

        this.items = [
            {label: 'Company', icon: 'pi pi-fw pi-home', routerLink: '/routing'},
            {label: 'Account', icon: 'pi pi-fw pi-user', routerLink: '/account'},
            {label: 'Operation', icon: 'pi pi-fw pi-send', routerLink: '/operation'}
        ];
    }

    show() {
        const ref = this.dialogService.open(InputComponent, {
            header: 'Choose',
            width: '70%'
        });
    }
}
