import { Component, OnInit } from '@angular/core';
import { FilterService } from 'primeng/api';
import { CountryService } from './countryservice';

// import { RoutingComponent } from '../routing/routing.component';
// import {DialogService} from 'primeng/dynamicdialog';
import {DialogService} from 'primeng/dynamicdialog';
import { RoutingComponent } from '../routing/routing.component';
import { HomeComponent } from '../home/home.component';


@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  providers: [CountryService, FilterService, DialogService],
  
})
export class InputComponent implements OnInit {


  countries: any[];
  cities: any[];
  filteredCountries: any[];


  gerant_name: any;
  gerant_titre: any;
  phone: any;
  email: any;
  contact_fact: any;
  contact_rh: any;
  raison_sociale_societe: any;
  siret_soc: any;
  fact_prefixe_soc: any;
  adresse: any;
  pays: any;
  constructor(
    private countryService: CountryService,
    public dialogService: DialogService,
    ) {
    this.cities = [
      { name: 'New York', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' },
    ];
  }
  ngOnInit(): void {
    this.countryService.getCountries().then((countries) => {
      this.countries = countries;
    });
  }
  
  searchCountry(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.countries.length; i++) {
      let country = this.countries[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }

    this.filteredCountries = filtered;
  }
  show() {
    const ref = this.dialogService.open(HomeComponent, {
        header: 'Choose',
        width: '70%'
    });
}

}


