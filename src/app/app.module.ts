import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/chips';
import { DropdownModule } from 'primeng/dropdown';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import {SidebarModule} from 'primeng/sidebar';
import {MenuModule} from 'primeng/menu';
import {TableModule} from 'primeng/table';

import {ToastModule} from 'primeng/toast';


import { AppRoutingModule } from './app-routing.module';



import { InputComponent } from './components/input/input.component';
import { RoutingComponent } from './components/routing/routing.component';
import { HomeComponent } from './components/home/home.component';
import { AccountComponent } from './components/account/account.component';
import { AddAccountComponent } from './components/account/add-account/add-account.component';
import { OperationComponent } from './components/account/operation/operation.component';

import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
// import {DynamicDialogModule} from 'primeng/dynamicdialog';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AutoCompleteModule,
    FormsModule,
    HttpClientModule,
    CalendarModule,
    ChipsModule,
    DropdownModule,
    InputMaskModule,
    InputNumberModule,
    CascadeSelectModule,
    MultiSelectModule,
    InputTextareaModule,
    InputTextModule,
    // DynamicDialogModule,
    SidebarModule,
    MenuModule,
    AppRoutingModule,
    TableModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    FormsModule,


  ],
  declarations: [AppComponent, InputComponent, RoutingComponent, HomeComponent, AccountComponent, AddAccountComponent, OperationComponent],
  bootstrap: [AppComponent],

  entryComponents: [
    RoutingComponent,
      InputComponent,
]
})
export class AppModule {}
